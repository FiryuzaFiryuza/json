package com.example.my.json.helpers;

import com.example.my.json.models.City;
import com.example.my.json.models.Country;
import com.example.my.json.models.District;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 123 on 26.09.2015.
 */
public class ParsingFunctions {

    public static List<Country> findCountryByCitiesCount(JSONObject object) {

        List<Country> countries = getCountries(object);

        if (countries != null && countries.size() > 0) {
            List<Country> defCountries = new ArrayList<>();
            for (Country country : countries) {
                if (country.getCities() != null && country.getCities().size() > 27) {
                    defCountries.add(country);
                }
            }
            return defCountries;
        }

        return null;
    }

    public static List<City> findCityByPopulation(JSONObject object) {

        List<Country> countries = getCountries(object);
        if (countries != null && countries.size() > 0) {

            List<City> cities = new ArrayList<>();
            for (Country country : countries) {
                if (country.getCities() != null && country.getCities().size() > 0) {
                    for (City city : country.getCities()) {
                        if (city.getPopulation() > 45000000) cities.add(city);
                    }
                }
            }
            return cities;
        }
        return null;
    }

    public static Country findBiggestCountry(JSONObject object) {

        List<Country> countries = getCountries(object);

        if (countries != null && countries.size() > 0) {
            long maxPopulation = 0;
            int biggestCountryPosition = -1;
            for (Country country : countries) {
                long population = countPopulation(country.getCities());
                if (population > maxPopulation) {
                    maxPopulation = population;
                    biggestCountryPosition = countries.indexOf(country);
                }
            }
            if (biggestCountryPosition > -1) return countries.get(biggestCountryPosition);

        }
        return null;
    }

    public static Country findCountryByLatitude(JSONObject response) {

        List<Country> countries = getCountries(response);
        if (countries != null && countries.size() > 0) {
            for (Country country : countries) {
                if (citiesHigherSixty(country.getCities())) return country;
            }
        }
        return null;
    }

    public static boolean hasCountryWithCitiesWithLargeDistricts(JSONObject response) {
        List<Country> countries = getCountries(response);
        if (countries != null && countries.size() > 0) {
            for (Country country : countries) {
                int cityCount = 0;
                if (country.getCities() != null && country.getCities().size() > 0) {
                    for (City city : country.getCities()) {
                        if (hasLargeDistricts(city)) {
                            cityCount++;
                            if (cityCount >= 2) return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private static boolean hasLargeDistricts(City city) {
        if (city.getDistricts() != null && city.getDistricts().size() > 0) {
            int districtsCount = 0;
            for (District district : city.getDistricts()) {
                if (district.getSize().equals(District.Size.LARGE)) districtsCount++;
            }
            if (districtsCount >= 7) return true;
        }
        return false;
    }

    private static boolean citiesHigherSixty(List<City> cities) {
        if (cities != null && cities.size() > 0) {
            for (City city : cities) {
                if (city.getLocation().getLatitude() < 60) {
                    return false;
                }
            }
        }
        return true;
    }

    private static long countPopulation(List<City> cities) {
        long population = 0;
        if (cities != null && cities.size() > 0) {
            for (City city : cities) {
                population += city.getPopulation();
            }
        }
        return population;
    }

    private static List<Country> getCountries(JSONObject object) {
        return ParseJSON.parseJson(object);
    }
}
