package com.example.my.json.models;

import android.location.Location;

import java.util.List;

/**
 * Created by 123 on 26.09.2015.
 */
public class City {

    public enum Marker {
        COUNTRY_CAPITAL,
        STATE_CENTER,
        WITH_AIRPORT,
        BUSINESS_CENTER,
        RESORT
    }

    private String name;
    private long population;
    private LatLng location;
    private List<District> districts;
    private List<Marker> markers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public List<Marker> getMarkers() {
        return markers;
    }

    public void setMarkers(List<Marker> markers) {
        this.markers = markers;
    }

}
