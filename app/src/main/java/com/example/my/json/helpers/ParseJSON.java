package com.example.my.json.helpers;

import com.example.my.json.models.City;
import com.example.my.json.models.Country;
import com.example.my.json.models.District;
import com.example.my.json.models.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 123 on 26.09.2015.
 */
public class ParseJSON {

    public static List<Country> parseJson(JSONObject response) {
        List<Country> countries = new ArrayList<>();

        try {
            JSONArray jsonCountries = response.getJSONArray("countries");
            for (int i = 0; i < jsonCountries.length(); i++) {
                countries.add(parseCountry(jsonCountries.getJSONObject(i)));
            }
        } catch (JSONException e) {
            return null;
        }
        return countries;
    }

    private static Country parseCountry(JSONObject jsonCountry) {
        try {
            Country country = new Country();
            country.setName(jsonCountry.getString("name"));
            country.setCode(jsonCountry.getString("code"));
            country.setCities(parseCities(jsonCountry.getJSONArray("cities")));
            return country;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<City> parseCities(JSONArray jsonCities) {
        List<City> cities = new ArrayList<>();
        for (int i = 0; i < jsonCities.length(); i++) {
            try {
                City city = new City();
                JSONObject jsonCity = jsonCities.getJSONObject(i);
                city.setName(jsonCity.getString("name"));
                city.setPopulation(jsonCity.getLong("population"));
                city.setLocation(parseLocation(jsonCity.getJSONObject("location")));
                city.setDistricts(parseDistricts(jsonCity.getJSONArray("districts")));
                city.setMarkers(parseMarkers(jsonCity.getJSONArray("markers")));
                cities.add(city);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return cities;
    }

    private static List<City.Marker> parseMarkers(JSONArray jsonMarkers) {
        List<City.Marker> markers = new ArrayList<>();
        for (int i = 0; i < jsonMarkers.length(); i++) {
            try {
                String markerName = jsonMarkers.getString(i);
                if (checkMarker(markerName)) {
                    markers.add(City.Marker.valueOf(markerName));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return markers;
    }

    private static boolean checkMarker(String markerName) {
        for (City.Marker marker: City.Marker.values()) {
            if (marker.toString().equals(markerName)) return true;
        }
        return false;
    }

    private static List<District> parseDistricts(JSONArray jsonDistricts) {
        List<District> districts = new ArrayList<>();
        for (int i = 0; i < jsonDistricts.length(); i++) {
            try {
                District district = new District();
                JSONObject jsonDistrict = jsonDistricts.getJSONObject(i);
                district.setName(jsonDistrict.getString("name"));
                district.setSize(District.Size.valueOf(jsonDistrict.getString("size")));
                districts.add(district);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return districts;
    }

    private static LatLng parseLocation(JSONObject jsonLocation) {
        try {
            LatLng location = new LatLng();
            location.setLatitude(jsonLocation.getDouble("latitude"));
            location.setLongitude(jsonLocation.getDouble("longitude"));
            return location;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
