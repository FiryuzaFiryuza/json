package com.example.my.json.models;

/**
 * Created by 123 on 26.09.2015.
 */
public class District {

    public enum Size {
        LARGE,
        MIDDLE,
        SMALL
    }

    private String name;
    private Size size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
