package com.example.my.json.models;

import java.util.List;

/**
 * Created by 123 on 26.09.2015.
 */
public class Country {

    private String name;
    private List<City> cities;
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
