package com.example.my.json;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.my.json.helpers.ParsingFunctions;
import com.example.my.json.models.City;
import com.example.my.json.models.Country;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class ApplicationTest  {

    private static final String JSON_FILE = "input.json";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testFindCountryByCitiesCount() throws JSONException {
        String expectedResults = "";

        List<Country> countries = ParsingFunctions.findCountryByCitiesCount(getJSONObj());
        assertEquals(0, countries.size());

        //assertEquals(expectedResults, countries.get(0).getName());
    }

    @Test
    public void testFindCityByPopulation() throws JSONException {
        String[] expectedResults = new String[0];

        List<City> cities = ParsingFunctions.findCityByPopulation(getJSONObj());

        assertEquals(expectedResults.length, cities.size());
        for (int j = 0; j < expectedResults.length; j++) {
            assertEquals(expectedResults[j], cities.get(j));

        }
    }

    @Test
    public void testFindBiggestCountry() throws JSONException {
        String[] expectedResults = { "Russia" };

        Country country = ParsingFunctions.findBiggestCountry(getJSONObj());

        assertEquals(expectedResults, country.getName());
    }

    @Test
    public void testFindCountryByLatitude() throws JSONException {
        String expectedResults = "Sample country 1";

        Country country = ParsingFunctions.findCountryByLatitude(getJSONObj());

        assertEquals(expectedResults, country.getName());
    }

    @Test
    public void testHasCountryWithCitiesWithLargeDistricts() throws JSONException {
        boolean expectedResults = false;

        boolean result = ParsingFunctions.hasCountryWithCitiesWithLargeDistricts(getJSONObj());
        assertEquals(expectedResults, result);
    }

    private JSONObject getJSONObj() throws JSONException {
        JSONObject jsonObjects = new JSONObject();

        jsonObjects = new JSONObject(mActivityRule.getActivity().getJsonFile(JSON_FILE));

        return jsonObjects;
    }
}